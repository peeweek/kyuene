prepare:
	@rm -rf .venv ;\
	python -m venv .venv ;\
	source .venv/bin/activate ;\
	pip install --upgrade pip ;\
	pip install -r requirements.txt

serve:
	@source .venv/bin/activate ;\
	./run.sh

wsgi:
	@source .venv/bin/activate ;\
	./wsgi.sh
