from flask import Flask, render_template, request, session
import yaml, os, hashlib


class Q:
    id : str
    author : str = ""
    text : str = ""
    score : int = 0
    upvotes = []
    downvotes = []
    answered : bool = False

    def __init__(self, author : str, question : str) :
        self.author = author
        self.text = question
        self.id = hashlib.sha1(author+question).hexdigest()
        self.upvotes = []
        self.downvotes = []
        self.score = 0
        self.answered = False

    def upvote(self, user_id:int):

        if(user_id in self.upvotes):
            return

        if(user_id in self.downvotes):
            self.downvotes.remove(user_id)

        self.upvotes.append(user_id)

        _update_score()

    def downvote(self, user_id:int):

        if(user_id in self.downvotes):
            return
        
        if(user_id in self.upvotes):
            self.upvotes.remove(user_id)

        self.downvotes.append(user_id)

        _update_score()

    def _update_score(self) :
        self.score = len(self.upvotes) - len(self.downvotes)        

questions = []

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
appconfig = None
with open("kyuene.conf.yml") as config_file:
    appconfig = yaml.safe_load(config_file)

# Secret key changes every run of the server
app.secret_key = os.urandom(24)

@app.route('/', methods=['POST', 'GET'])
def emptyRoute():
    return route("")

@app.route('/{room}', methods=['POST', 'GET'])
def route(room : str):

    action = request.form.get('action')
    
    username :str = None

    if action == "login" :
        username = request.form.get('username')
        session['username'] = username
    else :
        username = session.get('username')

    if username is None:
        return render_template('login.html', config=appconfig, questions=questions)
    else:
        if room == "":
            return render_template('room.html', config=appconfig, session=session)
        if action is None:        
            return render_template('index.html', config=appconfig, questions=questions, session=session)
        elif action == "login" :
            return render_template('index.html', config=appconfig, questions=questions, session=session)
        elif action == "ask" :
            question = request.form.get('question')
            questions.append(Q(username, question))
            return render_template('index.html', config=appconfig, questions=questions, session=session)

    return "This should not happen"
